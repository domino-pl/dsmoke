Disabling 8clk pleskaler of main clock for ATMEGA88 (result frequency 1MHz -> 8MHz)

Download low fuse
```sh
avrdude -c usbasp -pm88 -U lfuse:r:orginallfuse.hex
```
Convert hex to txt
```sh
xxd orginallfuse.hex > lfuse.txt
```

Edit `lfuse.txt` to change to set most importat bit as 1 (0x62 -> 0xe2)

Reverse conversion
```sh
xxd -r lfuse.txt > newlfuse.hex
```

Write new low fuse
```sh
avrdude -c usbasp -pm88 -U lfuse:w:newlfuse.hex
```