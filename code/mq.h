// Dominik Przybysz 20210609

#ifndef MQ_H_
#define MQ_H_

#include <stdint.h>
#include <stdbool.h>

#include "adc_item.h"

struct mq7_sensor{
	uint8_t second;
	
	volatile  uint16_t * power_output_ocr;
	adc_item * power_input_adc;
	adc_item * measure_adc;

	uint16_t last_low_volage_ocr;
	
	bool heating;
	bool detecting;
	bool alarm;
};
typedef struct mq7_sensor mq7_sensor;

void mq7_init(mq7_sensor * mq, volatile uint16_t * output_power_ocr, adc_item * input_power_adc, adc_item * measure_adc);
void mq7_engage(mq7_sensor * mq, const bool second);

struct mq2_sensor{
	volatile  uint8_t * heater_port;
	uint8_t heater_pn;
	
	adc_item * measure_adc;
	
	uint8_t preheating_second;
	
	bool preheating;	
	bool alarm;
};
typedef struct mq2_sensor mq2_sensor;

void mq2_init(mq2_sensor * mq, volatile uint8_t * heater_port, const uint8_t heater_pn, adc_item * measure_adc);
void mq2_start(mq2_sensor * mq);
void mq2_engage(mq2_sensor * mq, const bool second);


#endif // MQ_H_
