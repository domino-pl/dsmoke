// Dominik Przybysz 20210525
// Based on "mkuart.h" Mirosław Kardaś 20100904

#ifndef UART_H_
#define UART_H_

#include <stdint.h>
#include <stdbool.h>

#define UART_RX_BUF_SIZE 32 // receive buffer size
#define UART_TX_BUF_SIZE 200 // send buffer size

#define __UBRR(WANTED_BAUD) ((F_CPU+WANTED_BAUD*8UL) / (16UL*WANTED_BAUD)-1) // calculate UBRR value



void UART_Init( uint32_t baud );

bool uart_rx_available(void);  // any data in receive buffer
char uart_getc(void);  // read byte from receive buffer
void uart_putc( char data );  // put byte in send buffer
void uart_puts(char *s);  // put string to send buffer
void uart_putint(int value, int radix); // send integer as ASCII

bool uart_tx_buffer_empty(void); //check there ara any data in tx buffer

extern volatile bool uart_receive_buffer_overflow;

#endif /* UART_H_ */
