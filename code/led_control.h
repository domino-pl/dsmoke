// Dominik Przybysz 20210609

#ifndef LED_CONTROL_H_
#define LED_CONTROL_H_

#include <stdint.h>
#include <stdbool.h>

#define LED_OFF 0
#define LED_SOLID 1
#define LED_FAST 2
#define LED_SLOW 3
#define LED_FAST_COUNT 4
#define LED_SLOW_COUNT 5

struct led_control{
	volatile uint8_t * port;
	uint8_t pn;
	bool reverse;
	
	uint8_t mode;
	uint8_t after_counter_mode;
	
	uint8_t count_to;
	uint8_t counter_i;
};
typedef struct led_control led_control;

void led_init(led_control * led, volatile uint8_t * port, const uint8_t pn, const bool reverse);

void led_set(led_control * led, const uint8_t mode);
void led_set_counter(led_control * led, const uint8_t mode, const uint8_t times, const uint8_t after_counter_mode);

void led_blink(led_control * led, const uint8_t blink_type);


#endif // LED_CONTROL_H_
