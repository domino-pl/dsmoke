// Dominik Przybysz 20210609

#include "led_control.h"

void led_init(led_control * led, volatile uint8_t * port, const uint8_t pn, const bool reverse){
	led->port = port;
	led->pn = pn;
	led->reverse = reverse;
	
	led_set(led, LED_OFF);
}

void led_set(led_control * led, const uint8_t mode){
	led->mode = mode;
	switch(led->mode){
	case LED_OFF:
		if(led->reverse)
			*(led->port) |= 1<<led->pn;
		else
			*(led->port) &= ~(1<<led->pn);
	break;
	case LED_SOLID:
		if(led->reverse)
			*(led->port) &= ~(1<<led->pn);
		else
			*(led->port) |= 1<<led->pn;
	break;
	}
}

void led_set_counter(led_control * led, const uint8_t mode, const uint8_t times, const uint8_t after_counter_mode){
	led->counter_i = 0;
	led->count_to = 1+times*2;
	if(led->reverse)
		*(led->port) |= 1<<led->pn;
	else
		*(led->port) &= ~(1<<led->pn);
	led->mode = mode;
}

void led_blink(led_control * led, const uint8_t blink_type){
	if(led->mode == blink_type){
		*(led->port) ^= 1<<led->pn;
	}else if(
		(led->mode == LED_FAST_COUNT && blink_type == LED_FAST)
		||
		(led->mode == LED_SLOW_COUNT && blink_type == LED_SLOW)
	){
		if(led->counter_i == 0){
			if(led->reverse)
				*(led->port) |= 1<<led->pn;
			else
				*(led->port) &= ~(1<<led->pn);
		}else if(led->counter_i >= led->count_to){
			led_set(led, led->after_counter_mode);
		}else{
			*(led->port) ^= 1<<led->pn;
		}
		led->counter_i += 1;
	}
}
