// Dominik Przybysz 20210525
// Based on "mkuart.c" Mirosław Kardaś 20100904

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>


#include "uart.h"

#if MCU == atmega88
#define UBRRH UBRR0H
#define UBRRL UBRR0L
#define UCSRB UCSR0B
#define TXEN TXEN0
#define RXEN RXEN0
#define RXCIE RXCIE0
#define UDRIE UDRIE0
#define UDR UDR0
#define USART_RXC_vect USART_RX_vect
#endif

volatile bool uart_receive_buffer_overflow;

// receive buffor
volatile char UART_RxBuf[UART_RX_BUF_SIZE];
volatile uint8_t UART_RxHead; // buffor start position
volatile uint8_t UART_RxTail; // buffor end position

// transmit buffer
volatile char UART_TxBuf[UART_TX_BUF_SIZE];
volatile uint8_t UART_TxHead; // buffor start position
volatile uint8_t UART_TxTail; // buffor end position

void UART_Init( uint32_t baud) {
	uint16_t ubrr_value = __UBRR(baud);
	
	// setup speed
	UBRRH = (uint8_t)(ubrr_value>>8);
	UBRRL = (uint8_t)ubrr_value;
	
	// enable uart receive and transmit features
	UCSRB = (1<<RXEN)|(1<<TXEN);
	
	// default mode is 8bit 1 stop mode
	
	UCSRB |= (1<<RXCIE);
	
	uart_receive_buffer_overflow = 0;
}

// send byte
void uart_putc(char data) {
	uint8_t next_head;

    next_head = (UART_TxHead + 1);
	if(next_head >= UART_TX_BUF_SIZE)
		next_head = 0;
		
    // wait for empty space in buffer
    while(next_head == UART_TxTail);

    UART_TxBuf[next_head] = data;
    UART_TxHead = next_head;

    // enable Data Register Empty Interrupt called when next byte could be sent 
    UCSRB |= (1<<UDRIE);
}

// send string
void uart_puts(char * s) {
  register char c;
  while ((c = *s++)) // send until '\0' char
	uart_putc(c);
}

void uart_putint(int value, int radix)	// wysy³a na port szeregowy tekst
{
	char string[17];			// bufor na wynik funkcji itoa
	itoa(value, string, radix);		// konwersja value na ASCII
	uart_puts(string);			// wylij string na port szeregowy
}

// Data Register Empty Interrupt called when next byte could be sent 
ISR( USART_UDRE_vect)  {
    uint8_t next_TxTail;
    
    if (UART_TxHead != UART_TxTail) { // check if there are any data to send
    	next_TxTail = UART_TxTail + 1;
    	if(next_TxTail >= UART_TX_BUF_SIZE)
			next_TxTail = 0;
			
    	UDR = UART_TxBuf[next_TxTail]; // copy byte to send register
    	
    	UART_TxTail = next_TxTail;
    } else { // disable Data Register Empty Interrupt called when next byte could be sent 
		UCSRB &= ~(1<<UDRIE);
    }
}

bool uart_rx_available(void){
	return UART_RxHead != UART_RxTail;
}

// read one byte from receive buffer
char uart_getc(void) {
    char received_byte;
    uint8_t next_RxTail;
    
    if(!uart_rx_available())
		return 0;

    next_RxTail = (UART_RxTail + 1);
	if(next_RxTail >= UART_RX_BUF_SIZE)
		next_RxTail = 0;
		
    received_byte = UART_RxBuf[next_RxTail];
    UART_RxTail = next_RxTail;
    
    return received_byte;
}

// receive procedure
ISR( USART_RXC_vect ) {
    uint8_t next_head;
    char data;

    data = UDR; // read received byte

    next_head = ( UART_RxHead + 1);
	if(next_head >= UART_RX_BUF_SIZE)
		next_head = 0;

    // check space in receive buffer
    if(next_head == UART_RxTail) {
    	uart_receive_buffer_overflow = 1;
    } else {
		UART_RxBuf[next_head] = data;
		UART_RxHead = next_head;
    }
}

bool uart_tx_buffer_empty(void){
	return UART_TxHead == UART_TxTail;
}
