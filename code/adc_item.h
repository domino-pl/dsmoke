// Dominik Przybysz 20210609

#ifndef ADC_ITEM_H_
#define ADC_ITEM_H_

#ifndef ADC_V_REF
#define ADC_V_REF 5000
#endif

#include <stdint.h>
#include <stdbool.h>

struct adc_item{
	bool enabled;
	
	uint8_t adc_channel;
	
	uint16_t avg_n;
	uint32_t avg_sum;
	uint16_t avg_i;
	
	volatile uint16_t avg;	
	volatile bool complete;
	
	struct adc_item * next_item;
};
typedef struct adc_item adc_item;

void adc_item_init(adc_item * item, const uint8_t adc_channel, const uint16_t avg_n, const bool enabled, adc_item * next_item);
adc_item * adc_item_update(adc_item * item, const uint16_t read);

void adc_item_enabled(adc_item * item, const bool enabled);
bool adc_item_complete(const adc_item * item);
uint16_t adc_item_read(adc_item * item);

#endif // ADC_ITEM_H_
