// Dominik Przybysz 20210617

#ifndef DHT11_H_
#define DHT11_H_

#include <stdint.h>
#include <stdbool.h>
#include <avr/interrupt.h>

#define UART_RX_BUF_SIZE 32 // receive buffer size
#define UART_TX_BUF_SIZE 200 // send buffer size

#define __UBRR(WANTED_BAUD) ((F_CPU+WANTED_BAUD*8UL) / (16UL*WANTED_BAUD)-1) // calculate UBRR value

#define DHT11_STATE_IDLE 0
#define DHT11_STATE_PRE_LOW 1
#define DHT11_STETE_RECEIVING_HIGHT 2
#define DHT11_STETE_RECEIVING_LOW 3

struct dht11_sensor{
	uint8_t state;
	
	volatile uint8_t * port;
	volatile uint8_t * pin;
	volatile uint8_t * ddr;
	uint8_t pn;
	
	volatile uint8_t * counter;
	
	volatile bool new_read;
	
	uint8_t temp_int;
	uint8_t temp_dec;
	uint8_t hum_int;
	uint8_t hum_dec;

	volatile bool communication_error;
	volatile bool timeout_error;
	volatile bool checksum_error;
	
	volatile uint16_t time_us;
	uint64_t data;
	uint8_t received_i;
};
typedef struct dht11_sensor dht11_sensor;

void dht11_init(dht11_sensor * dht11, volatile uint8_t * port, volatile uint8_t * pin, volatile uint8_t * ddr, const uint8_t pn, volatile uint8_t * counter);

void dht11_init_measure(dht11_sensor * dht11);

void dht11_start_receiving(dht11_sensor * dht11);

//parse data
void dht11_parse(dht11_sensor * dht11);

// call on external interrupt
void dht11_it(dht11_sensor * dht11);
// call on timer overflow
void dht11_timer_overflow(dht11_sensor * dht11);

#endif /* DHT11_H_ */
