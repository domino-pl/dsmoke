// Dominik Przybysz 20210609

#ifndef DSMOKE_PINOUT_H_
#define DSMOKE_PINOUT_H_


#define MQ2_HEATER_DDR DDRB
#define MQ2_HEATER_PORT PORTB
#define MQ2_HEATER_PN PB0
#define MQ2_HEATER_PIN PINB

#define MQ7_HEATER_DDR DDRB
#define MQ7_HEATER_PORT PORTB
#define MQ7_HEATER_PN PB1
#define MQ7_HEATER_PIN PINB
#define MQ7_HEATER_PWM OCR1A
#define MQ7_HEATER_PWM_MAX 1023

#define BUZZER_DDR DDRB
#define BUZZER_PORT PORTB
#define BUZZER_PN PB2
#define BUZZER_PIN PINB

#define LED3_DDR DDRB
#define LED3_PORT PORTB
#define LED3_PN PB6
#define LED3_PIN PINB

#define LED4_DDR DDRB
#define LED4_PORT PORTB
#define LED4_PN PB7
#define LED4_PIN PINB

#define WIFI_STATUS_DDR LED4_DDR
#define WIFI_STATUS_PORT LED4_PORT
#define WIFI_STATUS_PN LED4_PN
#define WIFI_STATUS_PIN LED4_PIN

#define MEASURE_MQ7_DDR DDRC
#define MEASURE_MQ7_PORT PORTC 
#define MEASURE_MQ7_PN PC0
#define MEASURE_MQ7_PIN PIN
#define MEASURE_MQ7_ADC 0
 
#define MEASURE_MQ2_DDR DDRC
#define MEASURE_MQ2_PORT PORTC 
#define MEASURE_MQ2_PN PC1
#define MEASURE_MQ2_PIN PINC
#define MEASURE_MQ2_ADC 1

#define MEASURE_MQ7_POWER_DDR DDRC
#define MEASURE_MQ7_POWER_PORT PORTC 
#define MEASURE_MQ7_POWER_PN PC2
#define MEASURE_MQ7_POWER_PIN PINC
#define MEASURE_MQ7_POWER_ADC 2

#define MEASURE_BATT_DDR DDRC
#define MEASURE_BATT_PORT PORTC 
#define MEASURE_BATT_PN PC3
#define MEASURE_BATT_PIN PINC
#define MEASURE_BATT_ADC 3

#define MEASURE_AC_PRESENT_DDR DDRC
#define MEASURE_AC_PRESENT_PORT PORTC 
#define MEASURE_AC_PRESENT_PN PC4
#define MEASURE_AC_PRESENT_PIN PINC
#define MEASURE_AC_PRESENT_ADC 4

#define MEASURE_CHARGE_DDR DDRC
#define MEASURE_CHARGE_PORT PORTC 
#define MEASURE_CHARGE_PN PC5
#define MEASURE_CHARGE_PIN PINC
#define MEASURE_CHARGE_ADC 5

#define DHT11_DDR DDRD
#define DHT11_PORT PORTD
#define DHT11_PN PD2
#define DHT11_PIN PIND

#define DHT11_COUNTER TCNT2

#define SWITCH_DDR DDRD
#define SWITCH_PORT PORTD
#define SWITCH_PN PD3
#define SWITCH_PIN PIND

#define ENABLE_CHARGE_DDR DDRD
#define ENABLE_CHARGE_PORT PORTD
#define ENABLE_CHARGE_PN PD6
#define ENABLE_CHARGE_PIN PIND

#define ENABLE_BATTERY_TEST_DDR DDRD
#define ENABLE_BATTERY_TEST_PORT PORTD
#define ENABLE_BATTERY_TEST_PN PD7
#define ENABLE_BATTERY_TEST_PIN PIND

#define LED2_DDR DDRD
#define LED2_PORT PORTD
#define LED2_PN PD4
#define LED2_PIN PINd

#define LED1_DDR DDRD
#define LED1_PORT PORTD
#define LED1_PN PD5
#define LED1_PIN PIND

#endif // DSMOKE_PINOUT_H_
