// Dominik Przybysz 20210609

#include <avr/interrupt.h>
#include "adc_item.h"

void adc_item_init(adc_item * item, const uint8_t adc_channel, const uint16_t avg_n, const bool enabled, adc_item * next_item){
	item->enabled = enabled;
	
	item->adc_channel = adc_channel;
	
	item->avg_n = avg_n;
	item->avg_sum = 0;
	item->avg_i = 0;
	
	item->avg = 0;
	
	item->complete = false;
	
	item->next_item = next_item;
}

adc_item * adc_item_update(adc_item * item, const uint16_t read){
	adc_item * next_item;
	
	item->avg_sum += read;
	item->avg_i += 1;
	
	if(item->avg_i >= item->avg_n){
		item->avg = item->avg_sum/item->avg_n;
		
		item->avg_i = 0;
		item->avg_sum = 0;
		
		item->complete = true;
		
		next_item = item->next_item; 
		while(!next_item->enabled){
			// if there are no enabled items collect curret item secend time
			if(next_item == item)
				break;
			next_item = next_item->next_item;
		}
		
		return next_item;
	}
	
	return item;
}

void adc_item_enabled(adc_item * item, const bool enabled){
	if(enabled){
		cli();
		item->avg_i = 0;
		item->avg_sum = 0;
		sei();
		item->complete = false;
		item->enabled = true;
	}else{
		item->enabled = false;
	}
}

bool adc_item_complete(const adc_item * item){
	return item->complete;
}

uint16_t adc_item_read(adc_item * item){
	item->complete = false;
	return item->avg;
}
