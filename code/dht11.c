#include "dht11.h"

void dht11_init(dht11_sensor * dht11, volatile uint8_t * port, volatile uint8_t * pin, volatile uint8_t * ddr, const uint8_t pn, volatile uint8_t * counter){
	dht11->state = DHT11_STATE_IDLE;
	
	dht11->port = port;
	dht11->pin = pin;
	dht11->ddr = ddr;
	dht11->pn = pn;
	
	dht11->counter = counter;
	
	dht11->new_read = false;
	
	dht11->temp_dec = 0;
	dht11->temp_int = 0;
	dht11->hum_dec = 0;
	dht11->hum_int = 0;
	
	dht11->communication_error = false;
	dht11->checksum_error = false;
	
	dht11->data = 0;
	dht11->received_i = 0;
}

void dht11_init_measure(dht11_sensor * dht11){
	*dht11->ddr |= 1<<dht11->pn;
	*dht11->port &= ~(1<<dht11->pn);
	
	*dht11->counter = 0;
	dht11->time_us = 0;
	dht11->state = DHT11_STATE_PRE_LOW;
}

void dht11_start_receiving(dht11_sensor * dht11){
	dht11->received_i = 0;
	cli();
	*dht11->ddr &= ~(1<<dht11->pn);
	dht11->state = DHT11_STETE_RECEIVING_LOW;
	*dht11->counter = 0;
	sei();
}

void dht11_parse(dht11_sensor * dht11){

	
	uint8_t temp_dec = (uint8_t)(dht11->data>>8);
	uint8_t temp_int = (uint8_t)(dht11->data>>16);
	uint8_t hum_dec = (uint8_t)(dht11->data>>24);
	uint8_t hum_int = (uint8_t)(dht11->data>>32);
	
	if(temp_dec+temp_int+hum_dec+hum_int != (uint8_t)(dht11->data)){
		dht11->checksum_error = true;
		return;
	}
	
	dht11->temp_int = temp_int;
	dht11->temp_dec = temp_dec;
	dht11->hum_dec = hum_dec;
	dht11->hum_int = hum_int;
}

void dht11_it(dht11_sensor * dht11){	
	if(dht11->state == DHT11_STETE_RECEIVING_HIGHT){
		if( ( (*(dht11->pin)) & (1<<dht11->pn)) ){
			dht11->communication_error = true;
		}
		
		dht11->data <<= 1;
		
		if(*dht11->counter >= 28+(70-28)/2)
			dht11->data |= 1;

		*dht11->counter = 0;
		
		dht11->received_i += 1;
		
		if(dht11->received_i == 5*8+2){
			dht11_parse(dht11);
			dht11->state = DHT11_STATE_IDLE;
		}else{
			dht11->state = DHT11_STETE_RECEIVING_LOW;
		}
	}else if(dht11->state == DHT11_STETE_RECEIVING_LOW){	
		dht11->state = DHT11_STETE_RECEIVING_HIGHT;
		*dht11->counter = 0;
	}
}

void dht11_timer_overflow(dht11_sensor * dht11){
	if(dht11->state == DHT11_STATE_PRE_LOW){
		dht11->time_us += 256;
		
		if(dht11->time_us >= 20000){
			dht11_start_receiving(dht11);
			dht11->time_us = 0;
		}
	}
	
	if(dht11->state == DHT11_STETE_RECEIVING_HIGHT || dht11->state == DHT11_STETE_RECEIVING_LOW){
		if(dht11->received_i < 2){
			dht11->time_us += 256;
			if(dht11->time_us >= 5000){
				dht11->timeout_error = true;
				dht11->state = DHT11_STATE_IDLE;
			}
		}else{
			dht11->timeout_error = true;
			dht11->state = DHT11_STATE_IDLE;
		}
	}
}


