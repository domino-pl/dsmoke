// Dominik Przybysz 20210609

#include "dsmoke_setup.h"
#include "mq.h"

void mq7_init(mq7_sensor * mq, volatile uint16_t * output_power_ocr, adc_item * input_power_adc, adc_item * measure_adc){
	mq->second = 0;
	
	mq->power_output_ocr = output_power_ocr;
	mq->power_input_adc = input_power_adc;
	mq->measure_adc = measure_adc;
	
	mq->last_low_volage_ocr = MQ7_INIT_LOW_VOLTAGE_OCR;
	
	mq->heating = false;
	mq->detecting = false;
	mq->alarm = false; 
}

void mq7_engage(mq7_sensor * mq, const bool second){
	bool last_heating = mq->heating;
	
	if(second){
		mq->second += 1;
		if(mq->second >= MQ7_CYCLE_TIME)
			mq->second = 0;
	}
	
	if(mq->second <= MQ7_HEATING_TIME){
		mq->heating = true;
		mq->detecting = false;
	}else if(mq->second <= MQ7_HEATING_TIME+MQ7_COOLING_TIME){
		mq->heating = false;
		mq->detecting = false;
	}else{
		mq->heating = false;
		mq->detecting = true;
	}
	
	
	if(mq->heating != last_heating){
		if(mq->heating){
			*mq->power_output_ocr = 0;
		}else{
			*mq->power_output_ocr = mq->last_low_volage_ocr;
			adc_item_enabled(mq->power_input_adc, true);
		}
	}
	
	
	if(!mq->heating && adc_item_complete(mq->power_input_adc)){
		uint16_t read = adc_item_read(mq->power_input_adc);
		uint16_t voltage = (uint32_t)read*5000/1024;
		
		if(voltage <= 1500-MQ7_INIT_LOW_VOLTAGE_MARGIN_MV){
			*mq->power_output_ocr -= 1;
		}else if(voltage >= 1500+MQ7_INIT_LOW_VOLTAGE_MARGIN_MV){
			*mq->power_output_ocr += 1;
		}
		
		mq->last_low_volage_ocr = *mq->power_output_ocr;
	}
	
	if(mq->detecting && adc_item_complete(mq->measure_adc)){
		uint16_t read = adc_item_read(mq->measure_adc);
		uint16_t voltage = (uint32_t)read*5000/1024;
		
		mq->alarm = voltage >= MQ7_ALARM_VOLTAGE_MV;
	}
}




void mq2_init(mq2_sensor * mq, volatile uint8_t * heater_port, const uint8_t heater_pn, adc_item * measure_adc){
	mq->heater_port = heater_port;
	mq->heater_pn = heater_pn;
	mq->measure_adc = measure_adc;;
	
	
	mq->preheating_second = 0;
	mq->preheating = false;
	mq->alarm = false;
}
void mq2_start(mq2_sensor * mq){
	*mq->heater_port &= ~(1<<mq->heater_pn);
	mq->preheating = true;
	adc_item_enabled(mq->measure_adc, true);
}

void mq2_engage(mq2_sensor * mq, const bool second){
	if(mq->preheating){
		if(second){
			mq->preheating_second += 1;
			if(mq->preheating_second >= MQ2_PREHEATING_TIME){
				mq->preheating = false;
			}else{
				return;
			}
		}
	}
	
	uint16_t read = adc_item_read(mq->measure_adc);
	uint16_t voltage = (uint32_t)read*5000/1024;
	
	mq->alarm = voltage >= MQ2_ALARM_VOLTAGE_MV;
}
