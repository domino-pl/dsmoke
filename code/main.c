#define BAUDRATE 250000UL

#include "dsmoke_pinout.h"
#include "dsmoke_setup.h"
#include "uart.h"
#include "adc_item.h"
#include "led_control.h"
#include "mq.h"
#include "dht11.h"

#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdlib.h>
#include <stdio.h>

#define STATE_OK 0
#define STATE_ALARM 1
#define STATE_QUIET_ALARM 2
#define STATE_MISSED_ALARM 3

volatile bool full_second = false;

volatile bool switch_push = false;
volatile bool switch_pull = false;

led_control led_power;
led_control led_mq7;
led_control led_mq2;
led_control led_buzzer;

adc_item adc_mq7;
adc_item adc_mq2;
adc_item adc_mq7_power;
adc_item adc_batt;
adc_item adc_ac;
adc_item adc_charge;

mq7_sensor mq7;
mq2_sensor mq2;
dht11_sensor dht11;

ISR(TIMER0_COMPA_vect) {
	static uint16_t ms = 0;
	static uint32_t counter = 0;
	
	ms += TIMER_MS;
	counter += 1;
	
	if(counter%BLINK_FAST_TIMER_N == 0){
		led_blink(&led_power, LED_FAST);
		led_blink(&led_mq7, LED_FAST);
		led_blink(&led_mq2, LED_FAST);
	}
	if(counter%BLINK_SLOW_TIMER_N == 0){
		led_blink(&led_power, LED_SLOW);
		led_blink(&led_mq7, LED_SLOW);
		led_blink(&led_mq2, LED_SLOW);
	}
	if(counter%BUZZ_FAST_TIMER_N == 0)
		led_blink(&led_buzzer, LED_FAST);
	if(counter%BUZZ_SLOW_TIMER_N == 0)
		led_blink(&led_buzzer, LED_SLOW);
	
	if(counter == BLINK_FAST_TIMER_N*BLINK_SLOW_TIMER_N*BUZZ_FAST_TIMER_N*BUZZ_SLOW_TIMER_N-1){
		counter = 0;
	}
	
	if(ms >= 1000){
		full_second = 1;
		ms = ms%1000;
	}
}

ISR(TIMER2_OVF_vect){
	dht11_timer_overflow(&dht11);
}

ISR(ADC_vect) {
	static adc_item * item = &adc_batt;
	static uint16_t read = 0;
	
	read = ADCL;
	read |= ADCH << 8; 
	
	item = adc_item_update(item, read);
	
	ADMUX = (ADMUX&0xf0) | (item->adc_channel&0x0f);
}

ISR(INT1_vect){
	if(SWITCH_PIN & 1<<SWITCH_PN)
		switch_pull = true;
	else
		switch_push = true;
}

ISR(INT0_vect){
	dht11_it(&dht11);
}

void init(void){
	/** ------------- SWITCH CONFIG -------------- **/
	
	SWITCH_PORT |= 1<<SWITCH_PN; // switch pull up resistor
	EICRA |= 1<<ISC10; // any level change on INT1 exec interrupt
	EIMSK |= 1<<INT1; // INT1 interrupt enable
	
	/** ------------------------------------------ **/
	/** ------------- LED/BUZZER OUTPUTS IO SETUP  -------------  **/
	
	LED1_DDR |= 1<<LED1_PN;
	LED2_DDR |= 1<<LED2_PN;
	LED3_DDR |= 1<<LED3_PN;
	BUZZER_DDR |= 1<<BUZZER_PN;
	
	led_init(&led_power, &LED1_PORT, LED1_PN, 0);
	led_init(&led_mq7, &LED2_PORT, LED2_PN, 0);
	led_init(&led_mq2, &LED3_PORT, LED3_PN, 0);
	led_init(&led_buzzer, &BUZZER_PORT, BUZZER_PN, 0);
	
	/** ------------- TIMER COUNTER 0 (8BIT) AS TIME CONTROL SETUP  -------------  **/
	
	TIMSK0 |= 1<<OCIE0A; // interrupt on compare math in OCR0A
	OCR0A = 249; // interrep once per 32ms
	TCCR0B |= (1<<CS02) | (1<<CS00); // prescaler 1024
	
	/** -------------------------------------------------------------------------  **/
	/** ------------- DHT11  -------------  **/

	EICRA |= 1<<ISC00; // interrupt on any logical change on INT0
	EIMSK |= 1<<INT0; // enable INT0 interrupt
	
	TCCR2B |= 1<<CS21; // prescaler 8 = 1us
	TIMSK2 |= 1<<TOIE2; // enable overflow interrupt
	
	dht11_init(&dht11, &DHT11_PORT, &DHT11_PIN, &DHT11_DDR, DHT11_PN, &DHT11_COUNTER);
	
	/** ----------------------------------  **/
	/** ------------- MQ7 POWER CONTEROL WITH 10BIT FAST PWM ON TIMER 1(16BIT)  -------------  **/
	
	// MQ7 POWER CONTROL IO init
	MQ7_HEATER_PORT |= 1<<MQ7_HEATER_PN;  // initialise with hight state
	MQ7_HEATER_DDR |= 1<<MQ7_HEATER_PN;  // gipo input
	// 16 bit timer 1 for 10 bit resolution fast PWM for MQ-7 power adjust
	TCCR1A |= 1<<COM1A1; //  OC1A on compare match ( standard fast PWM )
	TCCR1A |= (1<<WGM12) | (1<<WGM11) | (1<<WGM10); // 10 bit fast PWM mode
	TCCR1B |= (1<<CS10); // prescaler 1
	MQ7_HEATER_PWM = MQ7_HEATER_PWM_MAX; // OCR1A = 1023;
	
	// MQ7 struct init
	mq7_init(&mq7, &MQ7_HEATER_PWM, &adc_mq7_power, &adc_mq7);
	
	/** -------------------------------------------------------------------------------------  **/
	/** ------------- MQ2 INIT  -------------  **/
	
	// MQ2 POWER CONTROL IO init
	MQ2_HEATER_PORT |= 1<<MQ2_HEATER_PN;  // initialise with hight state
	MQ2_HEATER_DDR |= 1<<MQ2_HEATER_PN;  // gipo input
	// MQ2 struct init
	mq2_init(&mq2, &MQ2_HEATER_PORT, MQ2_HEATER_PN, &adc_mq2);
	
	/** -------------------------------------  **/
	/** ------------- ADC  -------------  **/
	
	// adc items
	adc_item_init(&adc_mq7, MEASURE_MQ7_ADC, MEASURE_MQ7_AVG_N, 1, &adc_mq2);
	adc_item_init(&adc_mq2, MEASURE_MQ2_ADC, MEASURE_MQ2_AVG_N, 1, &adc_mq7_power);
	adc_item_init(&adc_mq7_power, MEASURE_MQ7_POWER_ADC, MEASURE_MQ7_POWER_AVG_N, 1, &adc_batt);
	adc_item_init(&adc_batt, MEASURE_BATT_ADC, MEASURE_BATT_AVG_N, 1, &adc_ac);
	adc_item_init(&adc_ac, MEASURE_AC_PRESENT_ADC, MEASURE_AC_PRESENT_AVG_N, 1, &adc_charge);
	adc_item_init(&adc_charge, MEASURE_CHARGE_ADC, MEASURE_CHARGE_AVG_N, 1, &adc_mq7);
	
	// adc converter setup
	ADMUX |= 1<<REFS0; // voltage reference - AVCC with external capacitor at AREF pin
	ADCSRA |= 1<<ADATE; // enable digital trigger (by default free running(continuous) mode)
	ADCSRA |= (1<<ADPS2) | (1<<ADPS1);  // prescaler 64: 8MHz/64 = 125kHz
	ADCSRA |= 1<<ADIE;  // ADC interrupt enable on conversion complete
	ADCSRA |= 1<<ADEN; // enable ADC
	ADCSRA |= 1<<ADSC; // start first conversion
	
	/** --------------------------------  **/
	/** ------------- UART  -------------  **/
	
	UART_Init(BAUDRATE);
	
	/** ---------------------------------  **/
	/** ------------- INTERRUPTS  -------------  **/
	
	sei();
	
	/** ---------------------------------------  **/
}


void report(void){
	static uint16_t r_ac_adc;
	static uint16_t r_vcharge_adc;
	static uint16_t r_mq2_adc;
	static uint16_t r_mq7_adc;
	static uint16_t r_vbatt_adc;
	static uint16_t r_vmq7_power;
	static uint16_t r_mg7_ocr;
	static char * r_mq7_status;
	static char * r_dh11_communitcation;
	static char * r_dh11_checksum;
	static char * r_dh11_timeout;
	
	if(uart_tx_buffer_empty()){
		char data[200];
		
		r_mq2_adc = (uint32_t)adc_mq2.avg*5000/1024;
		r_mq7_adc = (uint32_t)adc_mq7.avg*5000/1024;
		r_vbatt_adc = (uint32_t)adc_batt.avg*3*5000/1024;
		r_ac_adc = (uint32_t)adc_ac.avg*3*5000/1024;
		r_vcharge_adc = (uint32_t)adc_charge.avg*3*5000/1024;
		r_vmq7_power = (uint32_t)adc_mq7_power.avg*5000/1024;
		r_mg7_ocr = (uint32_t)MQ7_HEATER_PWM;
		if(mq7.heating){
			r_mq7_status = " heat";
		}else if(!mq7.detecting){
			r_mq7_status = " cool";
		}else{
			r_mq7_status = "check";
		}
		r_dh11_communitcation = dht11.communication_error ? "DHT11_COMM_ERR" : " dht11_comm_ok";
		r_dh11_checksum = dht11.checksum_error ? "DHT11_SUM_ERR" : " dht11_sum_ok";
		r_dh11_timeout = dht11.checksum_error ? "DHT11_TIME_ERR" : " dht11_time_ok";
		
		
		sprintf(data, "| MQ7(%s): %4dmV | MQ2: %4dmV | vMQ7: %4dmV(OCR=%4d) | vbatt: %5dmV | vac: %5dmV | vcharge: %5dmV | %s | %s | %s | temp: %3d | humidity: %3d |\r\n", r_mq7_status, r_mq7_adc, r_mq2_adc, r_vmq7_power, r_mg7_ocr, r_vbatt_adc, r_ac_adc, r_vcharge_adc, r_dh11_communitcation, r_dh11_checksum, r_dh11_timeout, dht11.temp_int, dht11.hum_int);
		uart_puts(data);
	}
}


void user_interface(const bool second, const bool switch_pressed){
	static uint8_t mq7_state = STATE_OK;
	static uint8_t mq2_state = STATE_OK;
	static uint8_t last_mq7_state = STATE_OK;
	static uint8_t last_mq2_state = STATE_OK;
	static uint16_t mq7_quiet_time = 0;
	static uint16_t mq2_quiet_time = 0;
	
	if(switch_pressed){
		switch(mq7_state){
		case STATE_ALARM:
			mq7_state = STATE_QUIET_ALARM;
			mq7_quiet_time = 0;
			break;
		case STATE_MISSED_ALARM:
			mq7_state = STATE_OK;
			break;
		}
		
		switch(mq2_state){
		case STATE_ALARM:
			mq2_state = STATE_QUIET_ALARM;
			mq2_quiet_time = 0;
			break;
		case STATE_MISSED_ALARM:
			mq2_state = STATE_OK;
			break;
		}
	}
	
	if(second){
		if(mq7_state == STATE_QUIET_ALARM){
			mq7_quiet_time += 1;
			if(mq7_quiet_time >= MQ7_ALARM_QUIET_TIME)
				mq7_state = STATE_ALARM;
		}
		if(mq2_state == STATE_QUIET_ALARM){
			mq2_quiet_time += 1;
			if(mq2_quiet_time >= MQ2_ALARM_QUIET_TIME)
				mq2_state = STATE_ALARM;
		}
	}
	
	
	if(mq7.alarm){
		switch(mq7_state){
			case STATE_OK:
				mq7_state = STATE_ALARM;
				break;
			case STATE_MISSED_ALARM:
				mq7_state = STATE_ALARM;
				break;
		}
	}else{
		switch(mq7_state){
			case STATE_ALARM:
				mq7_state = STATE_MISSED_ALARM;
				break; 
			case STATE_QUIET_ALARM:
				mq7_state = STATE_OK;
				break; 
		}
	}
	
	if(mq2.alarm){
		switch(mq2_state){
			case STATE_OK:
				mq2_state = STATE_ALARM;
				break;
			case STATE_MISSED_ALARM:
				mq2_state = STATE_ALARM;
				break;
		}
	}else{
		switch(mq2_state){
			case STATE_ALARM:
				mq2_state = STATE_MISSED_ALARM;
				break; 
			case STATE_QUIET_ALARM:
				mq2_state = STATE_OK;
				break; 
		}
	}
	
	if(last_mq7_state != mq7_state){
		switch(mq7_state){
			case STATE_OK:
				led_set(&led_mq7, LED_OFF);
				break;
			case STATE_ALARM:
				led_set(&led_mq7, LED_FAST);
				break; 
			case STATE_QUIET_ALARM:
				led_set(&led_mq7, LED_SOLID);
				break; 
			case STATE_MISSED_ALARM:
				led_set(&led_mq7, LED_SLOW);
				break;
		}
	}
	
	if(last_mq2_state != mq2_state){
		switch(mq2_state){
			case STATE_OK:
				led_set(&led_mq2, LED_OFF);
				break;
			case STATE_ALARM:
				led_set(&led_mq2, LED_FAST);
				break; 
			case STATE_QUIET_ALARM:
				led_set(&led_mq2, LED_SOLID);
				break; 
			case STATE_MISSED_ALARM:
				led_set(&led_mq2, LED_SLOW);
				break;
		}
	}
	
	if(last_mq7_state != mq7_state || last_mq2_state != mq2_state){
		if(mq7_state == STATE_ALARM || mq2_state == STATE_ALARM){
			led_set(&led_buzzer, LED_FAST);
		}else{
			led_set(&led_buzzer, LED_OFF);
		}
	}
	
	last_mq7_state = mq7_state;
	last_mq2_state = mq2_state;
}

void dht11_control(bool second){
	static uint8_t second_i = 0;
	
	if(!second){
		return;
	}
	
	second_i += 1;
	
	if(second_i % DHT11_INTERVAL != 0){
		return;
	}
	
	second_i = 0;
	
	dht11.communication_error = false;
	dht11.checksum_error = false;
	dht11.timeout_error = false;
	dht11_init_measure(&dht11);
}

int main(void) {
	_delay_ms(1000);
	init();
	
	uart_puts("DSMOKE v0.1\r\n");
	led_set_counter(&led_buzzer, LED_FAST_COUNT, 2, LED_OFF);
	led_set(&led_power, LED_SOLID);
	mq2_start(&mq2);
	
	while(1){
		bool second = full_second;
		full_second = false;
		bool switch_pressed = switch_push;
		switch_push = false;
		
		mq7_engage(&mq7, second);
		mq2_engage(&mq2, second);
		
		dht11_control(second);
		
		user_interface(second, switch_pressed);
		
		report();
		
		_delay_ms(50);
	}
 		
	
	return 0;
}
